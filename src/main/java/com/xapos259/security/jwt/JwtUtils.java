package com.xapos259.security.jwt;

import java.security.Signature;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import com.xapos259.security.service.UserDetailsImpl;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;

@Component
public class JwtUtils {

	private static final Logger logger = LoggerFactory.getLogger(JwtUtils.class);

	@Value("${com.app.jwtSecret}")
	private String jwtSecret;

	@Value("${com.app.jwtExpiration}")
	private int JwtExpirationMs;

	public String generateJwtToken(Authentication authentication) {
		UserDetailsImpl userPrincipal = (UserDetailsImpl) authentication.getPrincipal();
	
		
		return Jwts.builder().setSubject((userPrincipal.getUsername()))
				.setIssuedAt(new Date((new Date()).getTime() + JwtExpirationMs))
				.signWith(SignatureAlgorithm.HS512, jwtSecret).compact();
	}

	public String getUsernameFromJwt(String token) {
		return Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token)
				.getBody().getSubject();
	}
	
	public boolean validateJwtToken(String authToken) {
		try {
			Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken);
			return true;
		}catch (SignatureException err) {
			logger.error("Inavalid Jwt Signature : {}", err.getMessage());
		}catch (MalformedJwtException err) {
			logger.error("Inavalid Jwt Signature : {}", err.getMessage());
		}catch (ExpiredJwtException err) {
			logger.error("Inavalid Jwt Signature : {}", err.getMessage());
		}catch (UnsupportedJwtException err) {
			logger.error("Inavalid Jwt Signature : {}", err.getMessage());
		}catch (IllegalArgumentException err) {
			logger.error("Inavalid Jwt Signature : {}", err.getMessage());
		}
		return false;
	}
}
