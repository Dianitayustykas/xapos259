package com.xapos259.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.xapos259.model.Variant;

@Repository
public interface VariantRepository extends JpaRepository<Variant,Long>{

}
