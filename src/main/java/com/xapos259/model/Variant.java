package com.xapos259.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "variant")
public class Variant {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "variant_code")
	private String variantCode;
	
	@Column(name = "variant_name")
	private String variantName;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getvariantCode() {
		return variantCode;
	}

	public void setvariantCode(String variantCode) {
		this.variantCode = variantCode;
	}

	public String getvariantName() {
		return variantName;
	}

	public void setvariantName(String variantName) {
		this.variantName = variantName;
	}
}
